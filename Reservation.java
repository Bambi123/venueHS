package unsw.venues;

import java.time.LocalDate;
import org.json.JSONObject;

    /**
     * class Reservation
     * @author Thomas French
     * 
     * Implemented <code>Reservation<code> will create a reservationclass that represents the customers details.  The class Reservation will 
     * represent a placemark that is carried in each of the rooms in the system and used for determining availability.
     */

public class Reservation {

    private String id;
    private LocalDate startDate;
    private LocalDate endDate;

    public Reservation(String id, LocalDate startDate, LocalDate endDate){
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * method: listReservations
     * listReservations will make a JSONObject detailing the imformation concerning the reservation
     * @return JSONObject
     */

    public JSONObject listReservation(){

        JSONObject reservationDetails = new JSONObject();
        reservationDetails.put("id", this.id);
        reservationDetails.put("start", this.startDate);
        reservationDetails.put("end", this.endDate);
        return reservationDetails;

    }
    /**
     * method: getId
     * getId returns the associated id from the current booking
     * @return String
     */

    public String getId(){
        return this.id;
    }

    /**
     * method: getStartDate
     * getStartDate returns the associated start date from the current booking
     * @return LocalDate
     */

    public LocalDate getStartDate(){
        return this.startDate;
    }

    /**
     * method: getEndDate
     * getEndDate returns the associated end date from the current booking
     * @return LocalDate
     */

    public LocalDate getEndDate(){
        return this.endDate;
    }
    
}
