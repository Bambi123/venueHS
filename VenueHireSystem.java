/** Thomas French   z5206283    28th of June, 2020
 * COMP2511 - Assignment 1 - Venue Hire System
 */
package unsw.venues;

import java.time.LocalDate;
import java.util.Scanner;
//Added imports from java.util library
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * Venue Hire System for COMP2511.
 *
 * A basic prototype to serve as the "back-end" of a venue hire system. Input
 * and output is in JSON format.
 *
 * @author Robert Clifton-Everest
 *
 */
public class VenueHireSystem {

    private List<Venue> venues;
    private List<Booking> bookings; // shortcut to find bookings in the system

    /**
     * Constructs a venue hire system. Initially, the system contains no venues,
     * rooms, or bookings.
     */
    public VenueHireSystem() {

        this.venues = new ArrayList<Venue>();
        this.bookings = new ArrayList<Booking>();

    }

    /**
     * method: processCommand
     * @param json
     * processCommand is the main bulk of the JSONString parsing and code work.  From here we consider the different command
     * cases and react differently.  
     * @return void
     */

    private void processCommand(JSONObject json) {


        switch (json.getString("command")) {

        case "room":

            String venue = json.getString("venue");
            String room = json.getString("room");
            String size = json.getString("size");
            boolean roomStatus = addRoom(venue, room, size);
            if (!roomStatus){
                System.out.println(makeRejectedJSONObject().toString());
            }
            break;

        case "request":

            String id = json.getString("id");
            LocalDate start = LocalDate.parse(json.getString("start"));
            LocalDate end = LocalDate.parse(json.getString("end"));
            int small = json.getInt("small");
            int medium = json.getInt("medium");
            int large = json.getInt("large");
            JSONObject resultRequest = request(id, start, end, small, medium, large);
            System.out.println(resultRequest.toString(2));
            break;

        case "change":

            String idChange = json.getString("id");
            LocalDate startChange = LocalDate.parse(json.getString("start"));
            LocalDate endChange = LocalDate.parse(json.getString("end"));
            int smallChange = json.getInt("small");
            int mediumChange = json.getInt("medium");
            int largeChange = json.getInt("large");
            JSONObject resultChange = change(idChange, startChange, endChange, smallChange, mediumChange, largeChange);
            System.out.println(resultChange.toString(2));
            break;

        case "cancel":

            String idCancel = json.getString("id");
            boolean cancelStatus = cancel(idCancel);
            if (!cancelStatus){
                System.out.println(makeRejectedJSONObject().toString(2));
            }
            break;

        case "list":

            String listVenue = json.getString("venue");
            JSONArray listResult = list(listVenue);
            System.out.println(listResult.toString(2));
            break;

        }

    }

    /**
     * method: addRoom
     * @param venue
     * @param room
     * @param size
     * addRoom will add a new room to the database.  If the venue is also new, the database will generate en entire new venue 
     * and allocate the new room to it.
     * @return boolean
     */

    private boolean addRoom(String venue, String room, String size) {
        //If Venue does exist in database, make a new room and add it
        for(Venue v: this.venues){
            if (venue.equals(v.getVenueName())){ // inbuilt method for strings to see if they are equal
                //We also need to check if there is an existing room in this venue
                if (v.checkIfRoomExists(new Room(room, size))){
                    return false;
                }
                Room r = new Room(room, size);
                v.addRoom(r);
                return true;
            }
        }
        //If Venue doesnt exist in database, make a new venue and add to it
        Venue v = new Venue(venue);
        Room r = new Room(room, size);
        v.addRoom(r);
        this.venues.add(v);
        return true;
    }

    /**
     * method: request
     * @param id
     * @param start
     * @param end
     * @param small
     * @param medium
     * @param large
     * request will make a new class<Booking> to represent a customer order.  The class<Booking> will hold important details
     * such as the venue, the booked rooms and the start/end date of the order.
     * @return JSONObject
     */

    public JSONObject request(String id, LocalDate start, LocalDate end, int small, int medium, int large) {

        // find any accessible rooms and venues that we can accomodate the booking in
        Booking booking = new Booking(id, start, end);
        boolean bookingStatus = booking.findVenue(this.venues, small, medium, large);

        if (!bookingStatus){ // error check
            return makeRejectedJSONObject();
        }
        //Now we need to make a reservation tag and give it to all the rooms in the booking

        Reservation reservation = new Reservation(id, start, end);
        booking.makeReservation(reservation);
        bookings.add(booking); // watch this, could be bad, just adds to the list in this system
        return booking.makeSuccessJSONObject();
        
    }

    /**
     * method: change
     * @param id
     * @param start
     * @param end
     * @param small
     * @param medium
     * @param large
     * change will remove the class<Booking> from the current database of bookings, it will then attempt to place a new booking
     * using request  If it fails, the previous booking will be reallocated into the databse, if it succeeds, the new booking is
     * allocated into the database.
     * @return JSONObject
     */

    public JSONObject change(String id, LocalDate start, LocalDate end, int small, int medium, int large) {

        Booking oldBooking = findBooking(id);
        if (oldBooking == null){
            return makeRejectedJSONObject();
        }

        boolean removeReservationStatus = oldBooking.removeReservations();
        if (!removeReservationStatus){
            return makeRejectedJSONObject();
        }

        JSONObject newBookingJSON = request(id, start, end, small, medium, large);
        String newBookingStatus = newBookingJSON.getString("status");
        if (newBookingStatus.equals("rejected")){
            Reservation returnReservation = new Reservation(oldBooking.getId(), oldBooking.getStartDate(), oldBooking.getEndDate());
            oldBooking.makeReservation(returnReservation);
            return makeRejectedJSONObject();
        }
        this.bookings.remove(oldBooking);
        return newBookingJSON;

    }

    /**
     * method: cancel
     * @param id
     * cancel will remove the first booking instance in the database found with the id: id.  
     * @return boolean
     */

    public boolean cancel(String id){

        Booking cancelBooking = findBooking(id);
        if (cancelBooking == null){
            return false;
        }
        //once we've found the booking, remove all its reservations
        boolean cancelReservationStatus = cancelBooking.removeReservations();
        if (!cancelReservationStatus){
            return false;
        }
        return true;
    }
    /**
     * method: list
     * @param venueName
     * list will make a list of a given venue name.  This list consists the rooms in the venue and all the reservations connected
     * to the rooms in the venue.
     * @return JSONArray
     */

    public JSONArray list(String venueName){

        for (Venue v: this.venues){
            if (v.getVenueName().equals(venueName)){
                return v.listVenue();
            }
        }
        return null;

    }

    /**
     * method: findBooking
     * @param id
     * findBooking will take the id/name of a booking and attempt to find the booking in the current database of bookings
     * @return Booking
     */

    public Booking findBooking(String id){

        for (Booking b: this.bookings){
            if (b.getId().equals(id)){
                return b;
            }
        }
        return null;
    }

    /**
     * method: makeRejectedJSONObject
     * makeRejectedJSONObject makes a universal failure JSONObject used in many of the other methods.  Conventionally used when
     * another method fails
     * @return JSONObject
     */

    public JSONObject makeRejectedJSONObject(){

        JSONObject rejected = new JSONObject();
        rejected.put("status", "rejected");
        return rejected;

    }

    /**
     * method: visualiseDatabase
     * visualiseDatabase is not used in the final version of the code, is is simply a useful tool in debugging the code
     * @return void
     */

    public void visualiseDatabase(){

        System.out.println("\n\n\n\n\n\n\n\n\n\n");
        System.out.println("#############################################");
        System.out.println("Visualising Database.......");
        System.out.println("---------------------------------------------");
        System.out.println("Our current Venues:");
        for (Venue v: this.venues){
            System.out.print(v.toString());
        }
        System.out.println("---------------------------------------------");
        System.out.println("Our current Reservations:");
        for (Venue v: this.venues){
            for (Room r: v.getRooms()){
                System.out.println(r.toString());
            }
        }
        System.out.println("---------------------------------------------");
        System.out.println("End Visualising Database.......");
        System.out.println("#############################################");

    }

    /**
     * method: main
     * main is the front face of the program.  All json strings are read in through the scanner set up in this class.
     * @return void
     */

    public static void main(String[] args) {

        VenueHireSystem system = new VenueHireSystem();
        Scanner sc = new Scanner(System.in);

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (!line.trim().equals("")) {
                JSONObject command = new JSONObject(line);
                system.processCommand(command);
            }
        }
        sc.close();
  
    }

}
