package unsw.venues;

import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;
import org.json.JSONArray;

    /**
     * class Venue
     * @author Thomas French
     * 
     * Implemented <code>Venuecode> will create a Venue class that exists as a representation of a venue in the venue hire system.
     */

public class Venue {

    private String venueName;
    private List<Room> rooms;

    public Venue(String venueName){
        this.venueName = venueName;
        this.rooms = new ArrayList<Room>();
    }

    /**
     * method: listVenue
     * listVenue will cycle through all the rooms in this venue and make a JSONArray with all their details
     * @return JSONArray
     */

    public JSONArray listVenue(){

        JSONArray venueDetails = new JSONArray();
        for (Room r: this.rooms){
            venueDetails.put(r.listRoom());
        }
        return venueDetails;
    }

    /**
     * method: findAvailableRooms
     * findAvailableRooms searches through all the rooms associated in this venue and tests if they are available, if a number of rooms
     * are available, they are returned through the function
     * @return List<Room>
     */

    public List<Room> findAvailableRooms(int noSmall, int noMedium, int noLarge, LocalDate start, LocalDate end){
        
        List<Room> availableRooms = new ArrayList<Room>();
        for (Room r: this.rooms){

            if (noSmall > 0 && r.getSize().equals("small") && r.isRoomAvailable(start, end)){
                availableRooms.add(r);
                noSmall--;

            } else if (noMedium > 0 && r.getSize().equals("medium") && r.isRoomAvailable(start, end)){
                availableRooms.add(r);
                noMedium--;
                
            } else if (noLarge > 0 && r.getSize().equals("large") && r.isRoomAvailable(start, end)){
                availableRooms.add(r);
                noLarge--;
            }

        }

        //Success Case
        if (noSmall == 0 && noMedium == 0 && noLarge == 0){
            return availableRooms;
        }

        return null;
    }

    /**
     * method: getVenueName
     * getVenueName returns the name of the venue
     * @return String
     */

    public String getVenueName(){
        return this.venueName;
    }

    /** 
     * method: getRooms 
     * getRooms returns the list of rooms associated with this venue
     * @return List<Room>
     */

    public List<Room> getRooms(){
        return this.rooms;
    }

    /**
     * method: addRoom
     * @param r
     * addRoom takes room r and adds it to the list of associated rooms to this venue
     * @return void
     */

    public void addRoom(Room r){
        this.rooms.add(r);
    }

    /**
     * method: toString
     * toString translates the contents of the venue in a string format, useful for debugging
     * @return String
     */

    public String toString(){
        String s = "Venue: " + this.venueName + " has rooms:\n";
        for (Room r: this.rooms){
            s += "    Room: " + r.getName() + ", with Size: " + r.getSize() + "\n";
        }
        return s;

    }

    /**
     * method: checkIfRoomExists
     * @param r
     * checkIfRoomExists takes room r and attempts to find it in the rooms associated in the venue.  If it finds it, it returns a true
     * @return boolean
     */

    public boolean checkIfRoomExists(Room r){

        for (Room roomInList: this.rooms){
            if (r.isEqual(roomInList)){
                return true;
            }
        }
        return false;
    }

}
