package unsw.venues;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

    /**
     * class Room
     * @author Thomas French
     * 
     * Implemented <code>Room<code> will create a Room class that exists as a representation of a room in a venue that can be
     * used in the venue hire system
     */

public class Room {
    
    private String name;
    private String size;
    private List<Reservation> reservations;

    public Room(String name, String size){
        this.name = name;
        this.size = size;
        this.reservations = new ArrayList<Reservation>();
    }

    /**
     * method: listRoom
     * listRoom will make a list of rooms with their associated reservation int he form of a JSONObject
     * @return JSONObject
     */

    public JSONObject listRoom(){

        JSONObject roomDetails = new JSONObject();
        roomDetails.put("room", this.name);
        //now we need to find all the reservations for the rooms
        JSONArray roomReservations = new JSONArray();
        sortReservations();
        for (Reservation r: this.reservations){
            roomReservations.put(r.listReservation());
        }
        roomDetails.put("reservations", roomReservations);
        return roomDetails;
        
    }

    /**
     * method: sortReservations
     * sortReservations quickly sorts all the reservations in the list of reservations according to their start dates.
     * @return void
     */

    public void sortReservations(){
        this.reservations.sort((r1, r2) -> r1.getStartDate().compareTo(r2.getStartDate()));
    }

    /**
     * method: isRoomAvailable
     * @param startDate
     * @param endDate
     * isRoomAvailable cycles through the list of reservations in the room and returns a boolean status to whether it is/isn't
     * avalable.
     * @return boolean
     */

    public boolean isRoomAvailable(LocalDate startDate, LocalDate endDate){
        //A room is available if the end date of the new reservation is before the start of the current reservation
        // or
        //its available of the start date of the new reservation is after the end of the current reservation
        //this must be true for all reservations currently registered in this room
        for (Reservation reservation: this.reservations){
            LocalDate currentStart = reservation.getStartDate();
            LocalDate currentEnd = reservation.getEndDate();
            if (    !(endDate.isBefore(currentStart) || startDate.isAfter(currentEnd)) ){
                return false;
            }
        }
        return true;
    }

    /**
     * method: removeReservation
     * @param id
     * @param start
     * @param end
     * removeReservation will sort through the list of reservations for this room and remove any matching reservation classes.
     * @return boolean
     */

    public boolean removeReservations(String id, LocalDate start, LocalDate end){

        for (Reservation r: this.reservations){
            if (r.getId().equals(id) && r.getStartDate().isEqual(start) && r.getEndDate().isEqual(end)){
                this.reservations.remove(r);
                return true;
            }
        }

        return false;
    }

    /**
     * method: addReservation
     * @param r
     * addReservation will add a reservation r to the list of reservations in this room.
     * @return void
     */

    public void addReservation(Reservation r){
        this.reservations.add(r);
    }

    /**
     * method: getName
     * getName will return the name of the room.
     * @return String
     */

    public String getName(){
        return this.name;
    }

    /**
     * method: getSize
     * getSize will return the size of the room
     * @return String
     */

    public String getSize(){
        return this.size;
    }

    /**
     * method: getReservations
     * getReservations returns the list of reservations in this room
     * @return List<Reservation>
     */

    public List<Reservation> getReservations(){
        return this.reservations;
    }

    /**
     * method: isEqual
     * @param r
     * isEqual will determine whether or not this room is equal to a given input room r
     * @return boolean
     */

    public boolean isEqual(Room r){
        if (this.name.equals(r.getName()) && this.size.equals(r.getSize())){
            return true;
        }
        return false;
    }

    /** 
     * method: toString
     * toString will translate the contents of this room into a string format which is a useful tool for debugging the code.
     * @return String
     */

    public String toString(){
        String s = "Room: " + this.name + " has reservations: \n";
        for (Reservation r: this.reservations){
            s += "    Id: " + r.getId() + ", startDate: " + r.getStartDate() + ", endDate: " + r.getEndDate() + "\n";
        }
        return s;
    }

}
