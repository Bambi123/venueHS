package unsw.venues;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * class Booking
 * @author Thomas French
 * 
 * Implemented <code>Booking<code> will create a booking class that represents the customers details and their given venues and rooms
 */

public class Booking {
    
    private String id;
    private LocalDate startDate;
    private LocalDate endDate;
    private Venue eventVenue;
    private List<Room> eventRooms;

    public Booking(String id, LocalDate startDate, LocalDate endDate){
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.eventVenue = null;
        this.eventRooms = new ArrayList<Room>();

    }
    /**
     * method: findVenue
     * @param venuesDatabase
     * @param noSmall
     * @param noMedium
     * @param noLarge
     * findVenue will sort through all of the venues in the database that are passed down into the method and decide whether or not
     * this venue is available for a booking.
     * @return boolean
    */

    public boolean findVenue(List<Venue> venuesDatabase, int noSmall, int noMedium, int noLarge){

        for (Venue v: venuesDatabase){
            List<Room> roomsAvailable = v.findAvailableRooms(noSmall, noMedium, noLarge, startDate, endDate);
            if (roomsAvailable == null){ // failure case
                continue;
            }
            //If we get to here, we have found a venue v with roomsAvailable rooms
            this.eventVenue = v;
            this.eventRooms = roomsAvailable;
            return true;
        }
        return false;

    }

    /**
     * method: removeReservation
     * removeReservation will cycle through all the rooms found in this booking and remove all of the reservations found that 
     * match the details of the booking to be removed.
     * @return boolean
     */


    public boolean removeReservations(){

        for (Room r: this.eventRooms){
            boolean status = r.removeReservations(this.id, this.startDate, this.endDate);
            if (!status){
                return false;
            }
        }

        return true;
    }

    /**
     * method: makeSuccessJSONObject
     * makeSuccessJSONObject will develop a success jsonObject to be returned to the output given that the booking reuest was 
     * a success.
     * @return JSONObject
     */

    public JSONObject makeSuccessJSONObject(){

        JSONObject obj = new JSONObject();
        obj.put("status", "success");
        obj.put("venue", this.eventVenue.getVenueName());
        JSONArray rooms = new JSONArray();
        for (Room r: this.eventRooms){
            rooms.put(r.getName());
        }
        obj.put("rooms", rooms);
        return obj;
        
    }

    /**
     * method: getId
     * getID will find the id associated with this booking and return it
     * @return String
     */

    public String getId(){
        return this.id;
    }

    /**
     * method: getStartDate
     * getStartDate will find the start date associated with this booking and return it
     * @return LocalDate
     */

    public LocalDate getStartDate(){
        return this.startDate;
    }
        
    /**
     * method: getEndDate
     * getEndDate will find the end date associated with this booking and return it
     * @return LocalDate
     */

    public LocalDate getEndDate(){
        return this.endDate;
    }

    /**
     * method: getVenue
     * getVenue will find the venue associated with this booking and return it
     * @return Venue
     */

    public Venue getVenue(){
        return this.eventVenue;
    }

    /**
     * method: getRooms
     * getRooms will find the lsit of rooms associated with this booking and return it
     * @return List<Room>
     */

    public List<Room> getRooms(){
        return this.eventRooms;
    }

    /**
     * method: makeReservation
     * makeReservation will cycle through all the rooms associated with this booking and adda reservation to is.
     * @return void
     */

    public void makeReservation(Reservation reservation){
        for (Room r: this.eventRooms){
            r.addReservation(reservation);
        }
    }

}
